# Docstring:
"""
Initial Alignment Guardian: Signal Recycling X Alignment.

This guardian script is designed to align the signal recycling X cavity. It is 
the penultimate initial alignment sub-guardian. This means it runs after green 
arm alignment, infrared X arm alignment, and short arm Michelson alignment.

Author: Nathan Holland, A. Mullavey.
Date: 2019-05-13
Contact: nathan.holland@ligo.org

Modified: 2019-05-13 (Created).
Modified: 2019-05-16 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-17 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-20 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-21 (Some clean up, removing path changes).
Modified: 2019-05-22 (Changed structure to make it more self contained).
Modified: 2019-05-24 (Syntatic changes, debugging from lower level API).
Modified: 2019-05-31 (Added INIT state).
Modified: 2019-06-07 (Fixed ezca feedthrough for align_restore, and align_save
                      functions).
Modified: 2019-06-10 (Moved towards PEP8 style).
Modified: 2019-06-11 (Added remarks about cdsutils.avg)
Modified: 2019-06-12 (Added notifications).
Modified: 2019-07-01 (Added data collection).
Modified: 2019-07-02 (Modify data collection in final state).
"""


#------------------------------------------------------------------------------
# Imports:

# Import time, to ensure the EPICS refresh rate is adhered to.
import time

# Import the minimum number of needed guardian subutilities.
from guardian import GuardState, GuardStateDecorator, Node

# Import the class Optic from optic.py
# Source @
# /opt/rtcds/userapps/release/asc/l1/guardian/optic.py
from optic import Optic

# Import the class TrigServo from trigservo.py.
# Source @
# /opt/rtcds/userapps/release/asc/l1/guardian/trigservo.py
from trigservo import TrigServo

# Import the LSC matrices access.
# Source @
# /opt/rtcds/userapps/release/isc/l1/guardian/isclib/matrices.py
import isclib.matrices as matrix

# Import the function align_restore from new_align_restore.py.
# Source @
# /opt/rtcds/userapps/release/sus/common/scripts/new_align_restore.py
from new_align_restore import align_restore

# Import the function align_save from new_align_save.py.
# Source @
# /opt/rtcds/userapps/release/sus/common/scripts/align_save.py
from new_align_save import align_save

# Import cdsutils.avg
# You can import this, tested on GRD_TST node: 2019-06-11-16:25.
from cdsutils import avg as cds_avg

# Import the EzAvg class from
# /opt/rtcds/userapps/release/isc/l1/guardian/isclib/epics_averaging.py
from isclib.epics_average import EzAvg, ezavg

#------------------------------------------------------------------------------
# Script Variables:

# Nominal state for this guardian.
nominal = "SRX_ALIGN_IDLE"
request = nominal

# Convenient access for QUADs and HSTSs.
itmx = Optic("ITMX")
etmx = Optic("ETMX")
itmy = Optic("ITMY")
etmy = Optic("ETMY")
prm = Optic("PRM")
srm = Optic("SRM")

#------------------------------------------------------------------------------
# Nodes to manage
imc_node = Node("IMC_LOCK")
alsx_node = Node("ALS_XARM")
alsy_node = Node("ALS_YARM")

#------------------------------------------------------------------------------
# Parameters

srx_locked_threshold = 20    #TODO: Check threshold, un-normalized threshold is 6

#------------------------------------------------------------------------------
# Functions

def IMC_10W():
    return (imc_node.arrived and (ezca['IMC-IM4_TRANS_SUM_OUTPUT']>9) and (imc_node.state == 'LOCKED_10W'))

def SRX_locked():
    p_as_c = ezca['ASC-AS_C_SUM_OUTPUT']
    p_in = ezca['IMC-IM4_TRANS_SUM_OUTPUT']
    a_value = p_as_c/p_in
    return a_value >= srx_locked_threshold

def turn_off_wfs():
    ezca.switch('ASC-SRC1_P','INPUT','OFF')
    ezca.switch('ASC-SRC1_Y','INPUT','OFF')
    ezca.switch('ASC-SRC2_P','INPUT','OFF')
    ezca.switch('ASC-SRC2_Y','INPUT','OFF')
    # clear histories?
    #time.sleep(0.5)
    #ezca['SUS-PRM_M1_LOCK_P_RSET'] = 2
    #ezca['SUS-PRM_M1_LOCK_Y_RSET'] = 2

def turn_off_dc_centering():
    ezca.switch(":ASC-DC1_P", "INPUT", "OFF")
    ezca.switch(":ASC-DC1_Y", "INPUT", "OFF")
    ezca.switch(":ASC-DC2_P", "INPUT", "OFF")
    ezca.switch(":ASC-DC2_Y", "INPUT", "OFF")

def turn_off_srx_feedback():
     ezca.switch('LSC-SRCL','OUTPUT','FM2','OFF')
     ezca.switch('SUS-SR2_M2_LOCK_L', 'FM1', 'OFF')
     ezca.switch('SUS-SR2_M1_LOCK_L','INPUT','OFF')
     ezca['LSC-SRCL_TRAMP']=0
     ezca['LSC-SRCL_GAIN']=0
     time.sleep(2)            
     ezca['LSC-SRCL_RSET']=2
     ezca['SUS-SR2_M2_LOCK_L_RSET']=2
     ezca['SUS-SR2_M1_LOCK_L_RSET']=2

#------------------------------------------------------------------------------
# Decorators

class assert_imc_10W(GuardStateDecorator):
    def pre_exec(self):
        if not IMC_10W():
            return 'SET_POWER_TO_10W'

class assert_srx_locked(GuardStateDecorator):
    def pre_exec(self):
        if not SRX_locked():
            turn_off_wfs()
            turn_off_dc_centering()
            turn_off_srx_feedback()            
            return 'LOCK_SRX'

#------------------------------------------------------------------------------
#TODO

# Do we want to have this guardian set the input power or just assume it's at 10W?

# We now do SR2 to AS-C centering at the same time as WFS to SRM.
# At times it won't be worth doing individual SR2 alignment.


#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#                                 Guardian States
#------------------------------------------------------------------------------

# Initial state - Does nothing but is defined.
class INIT(GuardState):
    request = False
    index = 0
    
    
    # Main class method - Succeeds.
    def main(self):
        # Succeed.
        return True


#------------------------------------------------------------------------------

# Idle state - Does nothing, serves as the recovery state.
class SRX_ALIGN_IDLE(GuardState):
    request = True
    index = 2

    # Main class method - Sets a wait timer.
    def main(self):
        # Set a timer for 1 second.
        self.timer["wait"] = 1

    # Run class method - Wait for timer expiration.
    def run(self):
        # Check for expiration of wait timer.
        if self.timer["wait"]:
            # Succeed when timer expires.
            return True

#------------------------------------------------------------------------------
# Reset state - Resets the filters, matrices and optics altered in this
# guardian script.
class SRX_ALIGN_RESET(GuardState):
    goto = True
    request = False
    index = 1
    
    # Main class method - Reset.
    def main(self):
        # TODO: Gracefully unlock SRX.

        turn_off_wfs()
        turn_off_dc_centering()
        turn_off_srx_feedback()

        # Re-align optics.
        #etmx.align('P', ezca).
        #itmy.align('Y', ezca)
        #etmy.align('P', ezca)
        #prm.align('P', ezca)
        #prm.align('Y', ezca)
        srm.align('P', ezca)
        
        # Reset the SR2 optic align pitch and yaw filter modules.
        ezca.write(":SUS-SR2_M1_OPTICALIGN_P_TRAMP", 1)
        ezca.write(":SUS-SR2_M1_OPTICALIGN_Y_TRAMP", 1)
        
        # Reset the SRM optic align pitch and yaw ramp times.
        ezca.write(":SUS-SRM_M1_OPTICALIGN_P_TRAMP", 1)
        ezca.write(":SUS-SRM_M1_OPTICALIGN_Y_TRAMP", 1)
        
        # Reset LSC input matrix.
        matrix.lsc_input.zero(row = "SRCL")
        matrix.lsc_input["SRCL", "REFL_A_RF9_I"] = 0.42
        matrix.lsc_input["SRCL", "REFL_A_RF45_I"] = 1
        matrix.lsc_input.TRAMP = 0
        matrix.lsc_input.load()
        
        # Reset the 9 MHz I offset.
        ezca.write(":LSC-REFL_A_RF9_I_OFFSET", 3)
        ezca.switch(":LSC-REFL_A_RF9_I", "OFFSET", "OFF")
        
        # Reset the 9 MHz Q offset.
        ezca.write(":LSC-REFL_A_RF9_Q_OFFSET", 2.85)
        ezca.switch(":LSC-REFL_A_RF9_Q", "OFFSET", "OFF")

        # Reset the SRCL filter trigger.
        ezca.write(":LSC-SRCL_MASK_FM2", 1)

        # Reset the SRM and SR2 M1 LOCK L filter modules.
        for optic in ["SRM", "SR2"]:
            # Switch the input on, value as per ISC_LOCK DOWN.
            ezca.switch(":SUS-{0}_M1_LOCK_L".format(optic), "INPUT", "ON")
            # Switch the gain to 1, as per ISC_LOCK DOWN.
            ezca.write(":SUS-{0}_M1_LOCK_L_GAIN".format(optic), 1)
            # Switch on FMs 1 and 6.
            ezca.switch(":SUS-{0}_M2_LOCK_L".format(optic), "FM1", "FM6", "ON")

        # Reset the SRCL filter module.
        ezca.write(":LSC-SRCL_TRAMP", 1)
        ezca.write(":LSC-SRCL_GAIN", 0)
        
        # Reset the SRCL filter module trigger level.
        ezca.write(":LSC-SRCL_TRIG_THRESH_ON", 150)
        
        # Reset the SRCL filters.
        ezca.switch(":LSC-SRCL", "FM3", "FM7", "FM10", "OUTPUT", "OFF")
        ezca.switch(":LSC-SRCL", "INPUT", "ON")

        # Reset SRC1 filters
        ezca.switch('ASC-SRC1_P', 'FM4', 'FM5', 'FM7', 'FM8', 'ON')
        ezca.switch('ASC-SRC1_Y', 'FM4', 'FM5', 'FM7', 'FM8', 'ON')
        ezca.switch('ASC-SRC2_P', 'FM2', 'FM5', 'ON')
        ezca.switch('ASC-SRC2_Y', 'FM2', 'FM5', 'ON')

        # Turn off SRM M1 integrator FIXME Should these be on?
        ezca.switch('SUS-SRM_M1_LOCK_P','FM1','OFF')
        ezca.switch('SUS-SRM_M1_LOCK_Y','FM1','OFF')
        
        # Reset the SRCL filter memory.
        ezca.write(":LSC-SRCL_RSET", 2)

        # Reset the SR2 and SRM M2 LOCK L filter memory.
        ezca.write(":SUS-SRM_M2_LOCK_L_RSET", 2)
        ezca.write(":SUS-SR2_M2_LOCK_L_RSET", 2)
        ezca.write(":SUS-SR2_M1_LOCK_P_RSET", 2)
        ezca.write(":SUS-SR2_M1_LOCK_Y_RSET", 2)

        # Reset SRM M2 DRIVE ALIGN L2L filter module.
        ezca.write(":SUS-SRM_M2_DRIVEALIGN_L2L_TRAMP", 1)
        ezca.write(":SUS-SRM_M2_DRIVEALIGN_L2L_GAIN", 0)

        # Reset SRM M3 DRIVE ALIGN L2L filter module.
        ezca.write(":SUS-SRM_M3_DRIVEALIGN_L2L_TRAMP", 3)
        ezca.write(":SUS-SRM_M3_DRIVEALIGN_L2L_GAIN", 0)
        
        # Reset SR2 M3 DRIVE ALIGN L2L filter module.
        ezca.write(":SUS-SR2_M3_DRIVEALIGN_L2L_TRAMP", 5)
        ezca.write(":SUS-SR2_M3_DRIVEALIGN_L2L_GAIN", 1.5)  #TODO Check this!
        #Reset as per IFO DOWN (ISC guardian) state.

        # Reset SR2 M2 DRIVE ALIGN L2L filter module.
        ezca.write(":SUS-SR2_M2_DRIVEALIGN_L2L_TRAMP", 1)
        ezca.write(":SUS-SR2_M2_DRIVEALIGN_L2L_GAIN", 1)
        ezca.switch(":SUS-SR2_M2_LOCK_L", "FM1", "OFF")

        #Reset the SR2 M1 P and Y filters (off in locking)
        for dof in ["P","Y"]:
            ezca.switch("SUS-SR2_M1_LOCK_{}".format(dof),"INPUT","OFF")
            ezca.write("SUS-SR2_M1_LOCK_{}_TRAMP".format(dof),5)
            time.sleep(0.3)
            ezca.write("SUS-SR2_M1_LOCK_{}_GAIN".format(dof),-0.0001)


        # TODO: Any additional resetting here.
        
        # Set a 5 second timer, for changes to take effect.
        self.timer["wait"] = 5
    
    # Run class method - Wait for timer to expire.
    def run(self):
        # Check for wait timer expiration.
        if self.timer["wait"]:
            # Succeed when timer expires.
            return True


#------------------------------------------------------------------------------
# Misalignment state -  Misalign optics so that only the SRX can be
# resonant. PRM and SRM should already be misaligned.
class MISALIGN_FOR_SINGLE_BOUNCE(GuardState):
    request = True
    index = 3
    
    # Main class method - Misalign optics.
    def main(self):

        alsx_node.set_request("QPDS_LOCKED")
        alsy_node.set_request("QPDS_LOCKED")

        # Misalign ETMX, ITMY, ETMY, PRM, SRM
        etmx.misalign('P', -40, ezca)
        itmy.misalign('Y', -80, ezca)
        etmy.misalign('P', -40, ezca)
        prm.misalign('P', 1500, ezca)
        prm.misalign('Y', 1500, ezca)
        srm.misalign('P', -1500, ezca)

        # Set a 3 second timer, for changes to take effect.
        self.timer["wait"] = 3

    # Run class method - Wait for the changes to take effect.
    def run(self):
        # Check for wait timer expiration.
        if self.timer["wait"]:
            # Succeed when timer expires.
            return True

#------------------------------------------------------------------------------
# Set Power to 10W if not already there
class SET_POWER_TO_10W(GuardState):
    request = False
    index = 7

    def main(self):

        # Need to do this to steal control?
        imc_node.set_managed()
        # Request 10W (maybe better to use IFO POWER state?)
        imc_node.set_request("LOCKED_10W")
        # Release control, no need to micromanage
        imc_node.release()

        # Set a timer to periodically check IMC status 
        self.timer['check'] = 2

        # A timer to tell the operator that IMC is taking too long
        self.timer['timeout'] = 120

    def run(self):

        if self.timer['timeout']:
            notify("IMC_LOCKED guardian has failed to reach \"LOCKED_10W\" "+ \
                   "within 120 seconds (timeout), please investigate.")

        if not self.timer['check']:
            return

        # Check every two seconds
        if not IMC_10W():
            self.timer['check'] = 2
            return

        return True

#------------------------------------------------------------------------------
class AS_C_TO_SR2_SERVO(GuardState):
    request = False
    index = 9

    def main(self):

        ###
        # The matrices and filter bank should be set up, but in case they're not:
        sens = 'AS_C_DC'
        dof = 'SRC1'
        act = 'SR2'

        #Set the input matrix
        matrix.asc_input_pit.zero(row=dof)
        matrix.asc_input_yaw.zero(row=dof)
        matrix.asc_input_pit[dof,sens] = 1
        matrix.asc_input_yaw[dof,sens] = 1

        #Set the output matrix
        matrix.asc_output_pit.zero(col=dof)
        matrix.asc_output_yaw.zero(col=dof)
        matrix.asc_output_pit[act,dof] = 1
        matrix.asc_output_yaw[act,dof] = 1

        #Set the SR2 M1 filters (kept off in locking now, but we need them in alignment, ajm220104)
        for dof in ["P","Y"]:
            ezca.write("SUS-SR2_M1_LOCK_{}_TRAMP".format(dof),0.1)
            time.sleep(0.3)
            ezca.write("SUS-SR2_M1_LOCK_{}_GAIN".format(dof),-1.0)
            ezca.switch("SUS-SR2_M1_LOCK_{}".format(dof),"INPUT","ON")

        #Set the filter (filters should be on already, we want FM7 off though)
        ezca.switch('ASC-SRC1_P','FMALL','INPUT','OFF','OUTPUT','ON')
        ezca.switch('ASC-SRC1_Y','FMALL','INPUT','OFF','OUTPUT','ON')
        ezca.switch('ASC-SRC1_P','FM4','FM5','FM8','ON')
        ezca.switch('ASC-SRC1_Y','FM4','FM5','FM8','ON')
        ezca['ASC-SRC1_P_GAIN'] = 2000
        ezca['ASC-SRC1_Y_GAIN'] = 2000
        time.sleep(3)
        ###


        # Engage feedback
        ezca.switch('ASC-SRC1_P','INPUT','ON')
        ezca.switch('ASC-SRC1_Y','INPUT','ON')

        self.chans = []
        for dof in ['PIT','YAW','SUM']:
            self.chans.append('ASC-AS_C_{}_OUTPUT'.format(dof))

        self.timer['wait'] = 10

        self.srxascAvgs = EzAvg(ezca,30,self.chans)

    def run(self):

        #if not self.timer['wait']:
        #    return

        #avgs = cds_avg(3,self.chans)

        [done,vals] = self.srxascAvgs.ezAvg()
        if not done:
            notify('Filling up EzAvg buffer')
            return

        as_c_pit = vals[0] #avgs[0]
        as_c_yaw = vals[1] #avgs[1]
        as_c_sum = vals[2] #avgs[2]
        p_in = ezca['IMC-IM4_TRANS_SUM_OUTPUT']

        # Wait until spot it centered. TODO: check as_c_sum threshold
        if abs(as_c_pit)>0.1 or abs(as_c_yaw)>0.1 or (as_c_sum/p_in)<1:
            return

        return True

#------------------------------------------------------------------------------
'''
class OFFLOAD_SR2_ASC_CONTROL(GuardState):
    request = False
    index = 13

    #@assert_light_on_as_c
    def main(self):

        # Time to offload
        t_offload = 5

        ezca['SUS-SR2_M1_OPTICALIGN_P_TRAMP'] = t_offload
        ezca['SUS-SR2_M1_OPTICALIGN_Y_TRAMP'] = t_offload

        calP = ezca['SUS-SR2_M1_OPTICALIGN_P_GAIN']
        calY = ezca['SUS-SR2_M1_OPTICALIGN_Y_GAIN']

        offloadP = cds_avg(-5,'SUS-SR2_M1_LOCK_P_OUTPUT')
        offloadY = cds_avg(-5,'SUS-SR2_M1_LOCK_Y_OUTPUT')

        ezca['SUS-SR2_M1_OPTICALIGN_P_OFFSET'] += offloadP/calP
        ezca['SUS-SR2_M1_OPTICALIGN_Y_OFFSET'] += offloadY/calY

        self.timer['offload'] = t_offload

    #@assert_light_on_as_c
    def run(self):

        if not self.timer['offload']:
            return
        # Do some checks, are we still locked?
        return True
'''
#------------------------------------------------------------------------------
# Manual SR2 Alignment state - State for manual SR2 alignment.
class MANUAL_SR2_ALIGNMENT(GuardState):
    request = False
    index = 8
        
    # Main class method - Set up monitoring for transition back to
    # automatic alignment.
    def main(self):
        # TODO: Setup for monitoring transition back to automatic
        # alignment.
        
        # TODO: Setup for automatic scanning procedure.

        # Set a 15 second timer to remind the operator it is in manual
        # alignment.
        self.timer["remind"] = 15

    # Run class method - Remind operator and monitor for transition.
    def run(self):
        # Check for expiration of reminder timer.
        if self.timer["remind"]:

            # Remind the operator.
            notify("SR2 is in manual alignment mode.")
            log("SR2 is in manual alignment mode.")

            # Reset the timer.
            self.timer["remind"] = 15

        # TODO: Error checking.

        # TODO: Check conditions for transition back to automatic
        # alignment.

#------------------------------------------------------------------------------
# SR2 Aligned state - SR2 is now aligned
class SR2_ALIGNED(GuardState):
    request = True
    index = 10
    
    # Main class method - Save the new offset on SR2.
    def main(self):
        # Save the new offset.
        #align_save("SR2", ezca)

        # TODO: Setup to enforce that SR2 is aligned.

        # Flag for the first iteration of the run state.
        self.first_run = True
        
        # A timer for data collection.
        self.timer["collect"] = 10

    # Run class method - Enforce that SR2 is aligned.
    def run(self):
        # TODO: Use a GuardStateDecorator to enforce that SR2 is
        # aligned.
        # TODO: Error checking.

        #TODO: Check that SR2 is aligned.

#        # Check to see if it is the first run.
#        if self.first_run:
#            # Make sure further runs aren't the first.
#            self.first_run = False
#            
#            # Log the header.
#            header = "TRANS, PIT_ERR, YAW_ERR"
#            log(header)
#        
#        # Collect and log the data.
#        data = "DATA: {0:.3e}".format(ezca.read(":ASC-AS_C_SUM_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":ASC-AS_C_PIT_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":ASC-AS_C_YAW_OUTPUT"))
#        log(data)
        
        # Check for data collection timeout.
        if self.timer["collect"]:
            # Succeed.
            return True

#------------------------------------------------------------------------------
class PREPARE_SRX(GuardState):
    request = False
    index = 18

    def main(self):
        
        # Turn off AS-C feedback to SR2
        log('Turning off AS-C to SR2 servo.')
        ezca.switch('ASC-SRC1_P','INPUT','OFF')
        ezca.switch('ASC-SRC1_Y','INPUT','OFF')

        # Clear Histories
        #ezca['ASC-SRC1_P_RSET'] = 2
        #ezca['ASC-SRC1_Y_RSET'] = 2
        #ezca['SUS-SR2_M1_LOCK_P_RSET'] = 2
        #ezca['SUS-SR2_M1_LOCK_Y_RSET'] = 2

        # Realign the SRM. TODO: Here or next state?
        log('Realigning SRM')
        srm.align('P', ezca)

        self.timer['wait'] = 10

    def run(self):

        if not self.timer['wait']:
            return

        return True

#------------------------------------------------------------------------------
# SRX Lock Acquisition state - Acquire SRX lock.
class LOCK_SRX(GuardState):
    request = False
    index = 19

    @assert_imc_10W
    def main(self):
        # Realign the SRM.
        srm.align('P', ezca)

        # Adjust the LSC input matrix.
        matrix.lsc_input.zero(row = "SRCL")
        matrix.lsc_input["SRCL", "REFL_A_RF9_I"] = 1
        matrix.lsc_input.TRAMP = 0
        time.sleep(0.1)
        matrix.lsc_input.load()

        # Set the 9 MHz I demodulation offset. #TODO: Check this offset, need for PRX? No, usually turned back off
        ezca.write(":LSC-REFL_A_RF9_I_OFFSET", 3.0)
        ezca.switch(":LSC-REFL_A_RF9_I", "OFFSET", "ON")
        
        # Set the 9 MHz Q demodulation offset.
        ezca.write(":LSC-REFL_A_RF9_Q_OFFSET", 2.8)
        ezca.switch(":LSC-REFL_A_RF9_Q", "OFFSET", "ON")

        # Disable the triggering for SRCL FM2.
        ezca.write(":LSC-SRCL_MASK_FM2", 0)

        # Adjust the LOCK L filters.
        for optic in ["SRM", "SR2"]:
            #Disable input to SRM and SR2 M1 LOCK L filter.
            ezca.switch(":SUS-{0}_M1_LOCK_L".format(optic), "INPUT", "OFF")
            # Set the gain of the filter to 1.
            ezca.write(":SUS-{0}_M1_LOCK_L_GAIN".format(optic), 1)
            # Switch off the M2 LOCK L filters 1 and 6.
            ezca.switch(":SUS-{0}_M2_LOCK_L".format(optic), "FM1", "FM6",
                        "OFF")

        # Configure the SRCL filter.
        ezca.write(":LSC-SRCL_TRAMP", 0)
        ezca.write(":LSC-SRCL_GAIN", 0)
        ezca.switch(":LSC-SRCL", "FM3", "FM7", "FM10", "OUTPUT", "ON")
        ezca.switch(":LSC-SRCL", "FM2", "INPUT", "OFF")
        ezca.write(":LSC-SRCL_RSET", 2)

        # Reset the M2 LOCK L filters for SRM and SR2.
        ezca.write(":SUS-SRM_M2_LOCK_L_RSET", 2)
        ezca.write(":SUS-SR2_M2_LOCK_L_RSET", 2)

        # Set the DRIVEALIGN ramp times to 0 seconds.
        for optic in ["SRM", "SR2"]:
            for mass in ["M2", "M3"]:
                #Set LL ramp times.
                ezca.write(":SUS-{0}_{1}_DRIVEALIGN_L2L_TRAMP".format(optic,
                                                                      mass),
                           0)
        # Set the SRM DRIVEALIGN gains.
        ezca.write(":SUS-SRM_M2_DRIVEALIGN_L2L_GAIN", 0)
        ezca.write(":SUS-SRM_M3_DRIVEALIGN_L2L_GAIN", 1)
        # Set the SR2 DRIVEALIGN gains.
        ezca.write(":SUS-SR2_M2_DRIVEALIGN_L2L_GAIN", 1)
        ezca.write(":SUS-SR2_M3_DRIVEALIGN_L2L_GAIN", 1)

        # Remove SRCL triggering.
        ezca.write(":LSC-SRCL_TRIG_THRESH_ON", -10)
        
        # Lock SRX.
        ezca.write(":LSC-SRCL_TRAMP", 0)
        ezca.write(":LSC-SRCL_GAIN", -200000)
        ezca.switch(":LSC-SRCL", "INPUT", "ON")

        log("SRX locking.")

        # Flag to do items once.
        self.do_once = True
        # Set some timers
        self.timer["wait"] = 2		#Wait for changes to take effect

        self.srxAvgs = EzAvg(ezca,2,'ASC-AS_C_SUM_OUTPUT')

    @assert_imc_10W
    def run(self):

        if not self.timer["wait"]:
            return

        # Wait for cavity to lock.
        [done,power_as_c] = self.srxAvgs.ezAvg()
        if not done:
            notify('Filling up EzAvg buffer')
            return
        #power_as_c = cds_avg(2, 'ASC-AS_C_SUM_OUTPUT')
        p_in = ezca['IMC-IM4_TRANS_SUM_OUTPUT']
        if not (power_as_c/p_in) > srx_locked_threshold:
            notify('Waiting on SRX to lock')
            return

        # Check the initial alignment script condition.
        if self.do_once:
            # Unset the do once flag.
            self.do_once = False
            # Turn on the 1st filter on SR2 M2 LOCK L.
            ezca.switch(":SUS-SR2_M2_LOCK_L", "FM1", "ON")
            # Brief pause, present in source code.
            time.sleep(1)
            # Switch on the SRCL integrator.
            ezca.switch(":LSC-SRCL", "FM2", "ON")
            # Engage SR2 M1 offload (only needed if ground motion high)
            ezca.switch('SUS-SR2_M1_LOCK_L','INPUT','ON')
            # Log the end of SRX locking.
            log("SRX locked.")

        # Succeed.
        return True

#------------------------------------------------------------------------------
# SRX Locked state - Enforce that SRX is locked.
class SRX_LOCKED(GuardState):
    request = True
    index = 20
    
    # Main class method - Setup for lock checking.
    @assert_srx_locked
    def main(self):
        # TODO: Setup, similar to above, for improved SRX lock check.

        # Flag for first run iteration.
        self.first_run = True
        
        # Timer for data collection.
        self.timer["collect"] = 10
    
    # Run class method - Enforce that the SRX cavity is locked.
    @assert_srx_locked
    def run(self):
        # TODO: Use a GuardStateDecorator to enforce that SR2 is
        # aligned.
        # TODO: Execute checks to see that SRX is properly locked.

#        # Check for first iteration.
#        if self.first_run:
#            #Unset first run flag.
#            self.first_run = False
#
#            # Log the header for the data.
#            header = "TRANS, ERR"
#            log(header)
#        
#        # Log the data.
#        data = "DATA: {0:.3e}".format(ezca.read(":ASC-AS_C_SUM_OUTPUT")) + \
#               ", {0:.3e}".format(ezca.read(":LSC-REFL_A_RF9_I_OUTPUT"))
#        log(data)
        
        # Check for data collection timer expiration.
        if self.timer["collect"]:
            # Succeed.
            return True

#------------------------------------------------------------------------------
# Center the beams on the WFS.
class REFLWFS_DC_CENTERING(GuardState):
    request = False
    index = 27

    # Main class method - Setup for the WFS feedback.
    # TODO: First check that there is light on diodes, or do this above
    @assert_srx_locked
    def main(self):

        #TODO: Check for light on REFL WFS, maybe do this before locking SRX

        # Switch on the WFS centering.
        ezca.switch(":ASC-DC1_P", "INPUT", "ON")
        ezca.switch(":ASC-DC1_Y", "INPUT", "ON")
        ezca.switch(":ASC-DC2_P", "INPUT", "ON")
        ezca.switch(":ASC-DC2_Y", "INPUT", "ON")

        self.timer['wait'] = 5

    @assert_srx_locked
    def run(self):
        if not self.timer['wait']:
            return

        return True

#------------------------------------------------------------------------------
class REFLWFS_TO_SRM_SERVO(GuardState):
    request = False
    index = 28

    #@assert_reflwfscentered
    @assert_srx_locked
    def main(self):

        dof = 'SRC2'

        # Set the input matrix
        matrix.asc_input_pit.zero(row=dof)
        matrix.asc_input_yaw.zero(row=dof)
        matrix.asc_input_pit[dof,'REFL_B_RF9_I'] = 1
        matrix.asc_input_yaw[dof,'REFL_B_RF9_I'] = 1

        #Set the output matrix
        matrix.asc_output_pit.zero(col=dof)
        matrix.asc_output_yaw.zero(col=dof)
        matrix.asc_output_pit['SRM',dof] = 1
        matrix.asc_output_yaw['SRM',dof] = 1

        #Turn off all dof bank filters
        ezca.switch('ASC-'+dof+'_P','FMALL','INPUT','OFF','OUTPUT','ON')
        ezca.switch('ASC-'+dof+'_Y','FMALL','INPUT','OFF','OUTPUT','ON')

        #Make sure SRM integrators are on
        ezca.switch('SUS-SRM_M1_LOCK_P','FM1','ON')
        ezca.switch('SUS-SRM_M1_LOCK_Y','FM1','ON')

        #Set the PRC1 gains (sets how fast the PRM is aligned) #TODO: Find right gains
        ezca['ASC-'+dof+'_P_GAIN'] = 4000	#10000
        ezca['ASC-'+dof+'_Y_GAIN'] = 3000	#10000

        time.sleep(2)

        #Turn on servos
        ezca.switch('ASC-'+dof+'_P','INPUT','ON')
        ezca.switch('ASC-'+dof+'_Y','INPUT','ON')

        # Re-engage SR2 servos
        ezca.switch('ASC-SRC1_P','INPUT','ON')
        ezca.switch('ASC-SRC1_Y','INPUT','ON')

        self.timer['wait'] = 45
        self.once = True

        self.chans = []
        for sensor in ['AS_C','REFL_B_RF9_I']:
            for dof in ['PIT','YAW']:
                self.chans.append('ASC-{}_{}_OUTPUT'.format(sensor,dof))

        self.srxascAvgs = EzAvg(ezca,30,self.chans)

    #@assert_reflwfscentered
    @assert_srx_locked
    def run(self):

        [done,vals] = self.srxascAvgs.ezAvg()
        if not done:
            notify('Filling up EzAvg buffer')
            return

        #avgs = cds_avg(3,self.chans)

        as_c_pit = vals[0] #avgs[0]
        as_c_yaw = vals[1] #avgs[1]
        refl_wfs_pit = vals[2] #avgs[2]
        refl_wfs_yaw = vals[3] #avgs[3]
        p_in = ezca['IMC-IM4_TRANS_SUM_OUTPUT']

        if abs(refl_wfs_pit)>1.0 or abs(refl_wfs_yaw)>1.0:
            notify('waiting for WFS signals to zero')
            return

        # Wait until spot it centered. TODO: check as_c_sum threshold
        if abs(as_c_pit)>0.2 or abs(as_c_yaw)>0.2:
            notify('waiting for AS-C QPD to be centered')
            return

        # Wait 30 seconds after above conditions are met for better offloading
        #if self.once:
        #    self.once = False
        #    self.timer['wait'] = 30

        if not self.timer['wait']:
            return

        return True


#------------------------------------------------------------------------------
# Offload control signals to the alignment sliders.
class OFFLOAD_SRM_ASC_CONTROL(GuardState):
    request = False
    index = 29

    @assert_srx_locked
    def main(self):

        #optic = 'SRM'

        # Time to offload
        t_offload = 10

        chans = []
        
        for optic in ['SRM','SR2']:
            for dof in ['P','Y']:
                ezca['SUS-'+optic+'_M1_OPTICALIGN_'+dof+'_TRAMP'] = t_offload
                chans.append('SUS-'+optic+'_M1_LOCK_'+dof+'_OUTPUT')

        #offload = cds_avg(-5,chans)
        offload = ezavg(ezca,5,chans)
        cnt = 0

        # Offload the control signal to the suspension
        for optic in ['SRM','SR2']:
            for dof in ['P','Y']:
                cal = ezca['SUS-'+optic+'_M1_OPTICALIGN_'+dof+'_GAIN']
                #offload = cds_avg(-5,'SUS-'+optic+'_M1_LOCK_'+dof+'_OUTPUT')
                ezca['SUS-'+optic+'_M1_OPTICALIGN_'+dof+'_OFFSET'] += offload[cnt]/cal
                cnt += 1

        self.timer['offload'] = t_offload

    @assert_srx_locked
    def run(self):

        if not self.timer['offload']:
            return
        # Do some checks, are we still locked?
        return True

#------------------------------------------------------------------------------
class MANUAL_SRM_ALIGNMENT(GuardState):
    request = False
    index = 25
    
    # Main class method - Setup monitoring for transition back to
    # automatic SRM alignment.
    def main(self):
        # Switch off the WFS.
        turn_off_dc_centering()

        # TODO: Set up monitoring for the transition back to automatic
        # SRM alignment.

        # Set a timer to remind the operator every 15 seconds.
        self.timer["remind"] = 15

    # Run class method - Remind the operator and monitor for transition
    # back to the automatic alignment state.
    def run(self):
        # Check for reminder timer expiration.
        if self.timer["remind"]:
            #Remind the operator.
            notify("SRM is in manual alignment")
            log("SRM is in manual alignment")

            # Reset the remind timer.
            self.timer["remind"] = 15

        # TODO: Monitor for the transition back to automatic SRM
        # alignment.

#------------------------------------------------------------------------------
# SRM Aligned state - The final, complete state.
class SRM_ALIGNED(GuardState):
    request = True
    index = 30
    
    # Main class method - Setup to monitor SRX alignment.
    @assert_srx_locked
    def main(self):
        # Switch off the WFS. Should this be done if I want to monitor
        # the error signal? 
        #ezca.switch(":ASC-DC1_P", "INPUT", "OFF")
        #ezca.switch(":ASC-DC1_Y", "INPUT", "OFF")
        #ezca.switch(":ASC-DC2_P", "INPUT", "OFF")
        # ezca.switch(":ASC-DC2_Y", "INPUT", "OFF")

        # Save the position of SRM.
        align_save("SRM", ezca)
        align_save("SR2", ezca)

        # Set a flag for the first run of the run method.
        self.first_run = True
        
        # Set a timer to collect data.
        self.timer["collect"] = 10

    # Run class method - Monitor the alignment of SRX.
    def run(self):
        # TODO: Use a GuardStateDecorator to enforce that SR2 is
        # aligned.
        # TODO: Use a GuardStateDecorator to enforce that SRX is
        # locked.
        # TODO: Monitor the power in, and error signals for SRX to
        # ensure it remains aligned.

#        # Check for the first run.
#        if self.first_run:
#            # Set the first run flag to false.
#            self.first_run = False
#                        
#            # Log the header.
#            header = "TRANS, PIT_ERR, YAW_ERR"
#            log(header)
#        
#        # Log the data.
#        data = "DATA: {0:.3e}, ".format(ezca.read(":ASC-AS_C_SUM_OUTPUT")) + \
#               "{0:.3e}".format(ezca.read(":ASC-REFL_B_RF9_I_PIT_OUTMON")) + \
#               ", {0:.3e}".format(ezca.read(":ASC-REFL_B_RF9_I_YAW_OUTMON"))
        
        # Check that the data collection timer is running.
        #if not self.timer["collect"]:
        #    # Log the data.
        #    log(data)
        
        
        # Succeed.
        return True


#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#                                    Edges
#------------------------------------------------------------------------------

# Allowable transitions.
edges = [
         # Recovery.
         ("INIT", "SRX_ALIGN_IDLE"),
         ("SRX_ALIGN_RESET", "SRX_ALIGN_IDLE"),
         # Main - SR2 alignment.
         ("SRX_ALIGN_IDLE", "MISALIGN_FOR_SINGLE_BOUNCE"),
         ("MISALIGN_FOR_SINGLE_BOUNCE", "AS_C_TO_SR2_SERVO"),
         ("SET_POWER_TO_10W","AS_C_TO_SR2_SERVO"),
         ("MANUAL_SR2_ALIGNMENT", "AS_C_TO_SR2_SERVO"),
         ("AS_C_TO_SR2_SERVO", "SR2_ALIGNED"),
         #("OFFLOAD_SR2_ASC_CONTROL", "SR2_ALIGNED"),
         ("SR2_ALIGNED", "PREPARE_SRX"),
         # Main - SRX alignment.
         ("PREPARE_SRX", "LOCK_SRX"),
         ("LOCK_SRX", "SRX_LOCKED"),
         ("SRX_LOCKED", "REFLWFS_DC_CENTERING"),
         ("REFLWFS_DC_CENTERING", "REFLWFS_TO_SRM_SERVO"),
         ("REFLWFS_TO_SRM_SERVO", "OFFLOAD_SRM_ASC_CONTROL"),
         ("OFFLOAD_SRM_ASC_CONTROL", "SRM_ALIGNED"),
         ("MANUAL_SRM_ALIGNMENT", "REFLWFS_DC_CENTERING")
        ]


# END.
